import axios from "axios";
import { BASE_URL, configHeaders } from "./url.config";
import { userInforLocal } from "./local.service";
import { https } from "./url.config";
// url = domain + endpoint
// request = header + body
export const movieService = {
  getBanner: () => {
    // return axios({
    //   url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachBanner`,
    //   method: "GET",
    //   headers: configHeaders(),
    // });
    return https.get("/api/QuanLyPhim/LayDanhSachBanner");
  },
  getMovies: (params) => {
    // return axios({
    //   url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhimPhanTrang`,
    //   method: "GET",
    //   headers: configHeaders(),
    //   params,
    // });
    let uri = "/api/QuanLyPhim/LayDanhSachPhimPhanTrang";
    return https.get(uri, { params: params });
  },

  getMoviesByTheater: () => {
    // return axios({
    //   url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap`,
    //   method: "GET",
    //   headers: configHeaders(),
    // });
    let uri = "/api/QuanLyRap/LayThongTinLichChieuHeThongRap";
    return https.get(uri);
  },
  getInfoMovieSchedule: (maPhim) => {
    let uri = `/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${maPhim}`;
    return https.get(uri);
  },
};
