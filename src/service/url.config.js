import axios from "axios";
import { store } from "../index";
import { BAT_LOADING, TAT_LOADING } from "../redux/constants/spinnerConstant";
export const TOKEN_CYBERSOFT =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCDEkMOgIE7hurVuZyAwMyIsIkhldEhhblN0cmluZyI6IjAzLzAyLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY3NTM4MjQwMDAwMCIsIm5iZiI6MTY0NTgwODQwMCwiZXhwIjoxNjc1NTMwMDAwfQ.if3ZZ_VKK8nppxZJ2DF4FGoRPCmaYx6ncNAQytkjIT0";

export const BASE_URL = "https://movienew.cybersoft.edu.vn";

export const configHeaders = () => {
  return {
    TokenCybersoft: TOKEN_CYBERSOFT,
  };
};

export const https = axios.create({
  baseURL: BASE_URL,
  headers: configHeaders(),
});

// Add a request interceptor
https.interceptors.request.use(
  function (config) {
    store.dispatch({
      type: BAT_LOADING,
    });
    // Do something before request is sent
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
https.interceptors.response.use(
  function (response) {
    store.dispatch({
      type: TAT_LOADING,
    });
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  },
  function (error) {
    store.dispatch({
      type: TAT_LOADING,
    });
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  }
);
