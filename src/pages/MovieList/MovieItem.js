import React from "react";
import { Button } from "antd";
import { NavLink } from "react-router-dom";

export default function MovieItem(props) {
  const { hinhAnh, tenPhim, moTa, maPhim } = props.item;
  return (
    <div className="relative card border border-black p-4 rounded-lg ">
      <div className="card-top">
        <img src={hinhAnh} className="w-full h-60 object-cover" alt="" />
      </div>
      <div className="flex flex-col card-bottom">
        <h2 className="text-2xl text-white">{tenPhim}</h2>
        <p>{moTa.substr(0, 100) + "..."}</p>
        <NavLink
          className="absolute left-0 bottom-0 bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 "
          to={`detail/${maPhim}`}
        >
          Đặt vé
        </NavLink>
      </div>
    </div>
  );
}
