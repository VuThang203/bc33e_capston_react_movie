import React, { useEffect, useState } from "react";
import MovieItem from "./MovieItem";
import Slider from "react-slick";
import { movieService } from "../../service/movie.service";
import "../../css/movieList.css";

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", background: "red" }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", background: "green" }}
      onClick={onClick}
    />
  );
}
const MovieList = () => {
  const [movieData, setMovieData] = useState({});
  const fetchMovies = async (page = 1, pageSize = 10) => {
    const params = movieService.getMovies({
      maNhom: "GP01",
      soTrang: page,
      soPhanTuTrenTrang: pageSize,
    });
    try {
      const res = await movieService.getMovies(params);
      setMovieData(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };
  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    centerPadding: "60px",
    slidesToShow: 4,
    slidesToScroll: 3,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  useEffect(() => {
    fetchMovies({});
  }, []);

  return (
    <div className="my-16 mr-0">
      <Slider {...settings}>
        {movieData.items?.map((item) => (
          <MovieItem key={item.maPhim} item={item} />
        ))}
      </Slider>
    </div>
  );
};

export default MovieList;
