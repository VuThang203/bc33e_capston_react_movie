import React, { useState } from "react";
import "../../css/movie.css";
import FormInput from "./FormInput";
import { userServ } from "../../service/user.service";
import { useNavigate, NavLink } from "react-router-dom";
import { message } from "antd";

const Register = () => {
  let navigate = useNavigate();
  const [values, setValues] = useState({
    taiKhoan: "",
    email: "",
    matKhau: "",
  });

  const inputs = [
    {
      id: 1,
      name: "taiKhoan",
      type: "text",
      placeholder: "Tài khoản",
      errorMessage:
        "Account should be 3-16 characters and shouldn't include any special character!",
      label: "Tài Khoản",
      pattern: "^[A-Za-z0-9]{3,16}$",
      required: true,
    },
    {
      id: 2,
      name: "hoTen",
      type: "text",
      placeholder: "Họ Tên",
      label: "Họ Tên",
      pattern:
        "^[a-zA-Z_ÀÁÂÃÈÉÊẾÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêếìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]+$",
      errorMessage: "Name must be in correct format",
      required: true,
    },
    {
      id: 3,
      name: "email",
      type: "email",
      placeholder: "Email",
      errorMessage: "It should be a valid email address!",
      pattern: "^[w-.]+@([w-]+.)+[w-]{2,4}$",
      label: "Email",
      required: true,
    },
    {
      id: 4,
      name: "matKhau",
      type: "password",
      placeholder: "Mật Khẩu",
      errorMessage:
        "Password should be 8-20 characters and include at least 1 letter, 1 number and 1 special character!",
      label: "Mật khẩu",
      pattern: `^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$`,
      required: true,
    },
    {
      id: 5,
      name: "soDt",
      type: "phoneNumber",
      placeholder: "Số điện thoại",
      label: "SĐT",
      pattern: "^(84|0[3|5|7|8|9])+([0-9]{8})$",
      errorMessage: "Phone number must be in correct format",
      required: true,
    },
  ];

  const handleSubmit = (e) => {
    console.log(values);
    e.preventDefault();
    let handleRegister = async (dataRegister) => {
      try {
        let res = await userServ.postRegister(values);
        message.success("Đăng ký thành công");
        setTimeout(() => {
          navigate("/login");
        }, 1000);
      } catch (error) {
        console.log(error);
        message.error(error.response.data.content);
      }
    };
    handleRegister(values);
  };

  const onChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  return (
    <div
      className="movie "
      style={{
        backgroundImage: `url("https://images.pexels.com/photos/114979/pexels-photo-114979.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500")`,
      }}
    >
      <form className="formMovie" onSubmit={handleSubmit}>
        <h1 className="h1text text-2xl">Đăng ký</h1>
        {inputs.map((input) => (
          <FormInput
            key={input.id}
            {...input}
            value={values[input.name]}
            onChange={onChange}
          />
        ))}
        <button className="buttonMovie">Đăng nhập</button>
        <div className="mb-4">
          <span>Bạn đã có tài khoản? </span>
          <NavLink to="/login">Đăng nhập</NavLink>
        </div>
      </form>
    </div>
  );
};
export default Register;
