import React, { useRef, useEffect, useState } from "react";
import { Carousel, Button } from "antd";
import { movieService } from "../../service/movie.service";
import "./Carousel.css";

export default function HomeCarousel() {
  let ref = useRef();
  const [banners, setBanners] = useState([]);
  const fetchBanners = async () => {
    // call api=>banners
    // async await
    try {
      const res = await movieService.getBanner();
      setBanners(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };
  const arrowStyle = {
    top: "-480px",
    border: "none",
    fontSize: "80px",
    background: "none",
    color: "#456178",
  };
  useEffect(() => {
    fetchBanners();
  }, []);

  return (
    <div>
      <Carousel dots={false} ref={ref} autoplay={true} autoplaySpeed={5000}>
        {banners.map((item) => {
          return (
            <div key={item.maBanner}>
              <img
                src={item.hinhAnh}
                alt=""
                className="object-cover h-100 w-full"
              />
            </div>
          );
        })}
      </Carousel>
      <div className="relavtive bottom-96 flex justify-between">
        <Button
          style={arrowStyle}
          onClick={() => {
            ref.current.prev();
          }}
        >
          <i class="fa fa-angle-left"></i>
        </Button>
        <Button
          style={arrowStyle}
          className="items-right"
          onClick={() => {
            ref.current.next();
          }}
        >
          <i class="fa fa-angle-right"></i>
        </Button>
      </div>
    </div>
  );
}
