import React from "react";
import Header from "../../Components/Header/Header";
import HomeCarousel from "../Carousel/Carousel";
import MovieList from "../MovieList/MovieList";
import MoviesTabs from "../MovieTabs/MoviesTabs";
import Footer from "../../Components/Footer/Footer";

export default function HomePage() {
  return (
    <div>
      <Header />
      <HomeCarousel />
      <MovieList />
      <MoviesTabs />
      <Footer />
    </div>
  );
}
