import React from "react";
import moment from "moment";

export default function MovieTabItem({ movie }) {
  return (
    <div className="flex p-3 items-center">
      <img src={movie.hinhAnh} className="h-32 w-24 object-cover" alt="" />
      <div className="ml-5">
        <p className="text-left text-xl font-medium">{movie.tenPhim}</p>
        <div className="grid grid-cols-3 gap-5">
          {movie.lstLichChieuTheoPhim.slice(0, 6).map((item, index) => {
            return (
              <span key={index} className="p-3 bg-red-600 rounded text-white">
                {moment(item.ngayChieuGioChieu).format("DD/MM/YYYY - HH:MM")}
              </span>
            );
          })}
        </div>
      </div>
    </div>
  );
}
