import React, { useEffect, useState } from "react";
import { movieService } from "../../service/movie.service";
import { Tabs } from "antd";
import MovieTabItem from "./MovieTabItem";
import Spinner from "../../Components/Spinner/Spinner";

export default function MoviesTabs() {
  let [dataRaw, setDataRaw] = useState([]);

  //   const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    // setIsLoading(true);
    movieService
      .getMoviesByTheater()
      .then((res) => {
        // setIsLoading(false);
        console.log(res);
        setDataRaw(res.data.content);
      })
      .catch((err) => {
        // setIsLoading(false);
        console.log(err);
      });
  }, []);

  let renderHeThongRap = () => {
    return dataRaw.map((heThongRap, index) => {
      return (
        <Tabs.TabPane
          tab={<img className="w-20 h-20" src={heThongRap.logo} />}
          key={index}
        >
          <Tabs
            style={{ overflow: "scroll", height: "100vh" }}
            tabPosition="left"
            defaultActiveKey="1"
          >
            {heThongRap.lstCumRap.map((cumRap) => {
              return (
                <Tabs.TabPane
                  tab={<div className="w-60">{cumRap.tenCumRap}</div>}
                  key={cumRap.maCumRap}
                >
                  {cumRap.danhSachPhim.map((phim, index) => {
                    return <MovieTabItem key={index} movie={phim} />;
                  })}
                </Tabs.TabPane>
              );
            })}
          </Tabs>
        </Tabs.TabPane>
      );
    });
  };

  return (
    <div className="pb-20 container mt-20">
      {/* {isLoading && <Spinner />} */}
      <Tabs tabPosition="left" defaultActiveKey="1">
        {renderHeThongRap()}
      </Tabs>
    </div>
  );
}
