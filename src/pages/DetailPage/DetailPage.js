import Header from "Components/Header/Header";
import React, { useEffect } from "react";
import { Tabs } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { XEM_CHI_TIET_PHIM } from "redux/constants/filmConstant";
import { layThongTinChiTietPhim } from "redux/actions/filmAction";
import moment from "moment";

export default function DetailPage() {
  const filmDetail = useSelector((state) => state.filmReducer.filmDetail);
  console.log("filmDetail: ", filmDetail);
  const dispatch = useDispatch();
  let { id } = useParams();

  useEffect(() => {
    // lay thong tin param tu url
    dispatch(layThongTinChiTietPhim(id));
  }, []);

  // console.log("filmDetail: ", filmDetail);
  return (
    <div
    // style={{
    //   backgroundImage:
    //     "url('https://img6.thuthuatphanmem.vn/uploads/2022/03/16/background-phim-doat-giai_110939053.jpg')",
    // }}
    >
      <Header />
      <div
        style={{
          background: "rgba( 244, 223, 223, 0.1 )",
          boxShadow: "0 8px 32px 0 rgba( 31, 38, 135, 0.37 )",
          backdropFilter: "blur( 2.5px )",

          border: "1px solid rgba( 255, 255, 255, 0.18 )",
        }}
      >
        <div className="grid grid-cols-12">
          <div className="col-span-4 col-start-4">
            <div className="grid grid-cols-2">
              <img src={filmDetail.hinhAnh} alt="" />
              <div className="content">
                <p>
                  Ngày chiếu:
                  {moment(filmDetail.ngayKhoiChieu).format(
                    "DD/MM/YYYY - HH:MM"
                  )}
                </p>
                <h1 className="text-xl">{filmDetail.tenPhim}</h1>
                <p>{filmDetail.moTa}</p>
              </div>
            </div>
          </div>
        </div>
        <div className="mt-20 container">
          <Tabs tabPosition="left">
            {filmDetail.heThongRapChieu?.map((htr, index) => {
              return (
                <Tabs.TabPane
                  tab={
                    <div className="flex flex-col items-center">
                      <img width={50} height={50} src={htr.logo} alt="" />
                      {htr.tenHeThongRap}
                    </div>
                  }
                  key={index}
                >
                  {htr.cumRapChieu?.map((cumRap, index) => {
                    return (
                      <div key={index}>
                        <div className="flex flex-row">
                          <img
                            width={50}
                            height={50}
                            src={cumRap.hinhAnh}
                            alt=""
                          />
                          <div className="ml-3">
                            <h3>{cumRap.tenCumRap}</h3>
                          </div>
                        </div>
                        <div className="lichChieu grid grid-cols-4">
                          {cumRap.lichChieuPhim?.map((lichChieu, index) => {
                            return (
                              <div className="p-3 mt-3 bg-red-600 rounded text-white">
                                {moment(lichChieu.ngayChieuGioChieu).format(
                                  "DD/MM/YYYY - HH:MM"
                                )}
                              </div>
                            );
                          })}
                        </div>
                      </div>
                    );
                  })}
                </Tabs.TabPane>
              );
            })}
          </Tabs>
          ;
        </div>
      </div>
    </div>
  );
}
