import React from "react";
import { PacmanLoader, CircleLoader } from "react-spinners";
import { useSelector } from "react-redux";
import { spinnerReducer } from "../../redux/reducers/spinnerReducer";

export default function Spinner() {
  let { isLoading } = useSelector((state) => {
    return state.spinnerReducer;
  });
  return isLoading ? (
    <div className="fixed w-screen h-screen flex justify-center items-center bg-black top-0 left-0 z-50">
      <CircleLoader size={150} color="#36d7b7" />
    </div>
  ) : (
    <></>
  );
}
