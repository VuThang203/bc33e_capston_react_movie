import React from "react";
import UserNav from "./UserNav";

export default function Header() {
  return (
    <div className="shadow-lg sticky top-0 z-30 w-full px-2 py-2 bg-white sm:px-4 ">
      <div className="container mx-auto flex justify-between items-center h-20">
        <span className="logo font-bold text-red-600 text-2xl">CyberFlix</span>
        <div className="space-x-5 font-medium">
          <span>Lịch Chiếu</span>
          <span>Cụm Rạp</span>
          <span>Tin Tức</span>
          <span>Ứng Dụng</span>
        </div>
        <UserNav />
      </div>
    </div>
  );
}
