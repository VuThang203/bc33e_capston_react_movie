import { XEM_CHI_TIET_PHIM } from "redux/constants/filmConstant";
import { movieService } from "service/movie.service";

export const layThongTinChiTietPhim = (id) => {
  return async (dispatch) => {
    try {
      const res = await movieService.getInfoMovieSchedule(id);
      // dua len redux
      dispatch({
        type: XEM_CHI_TIET_PHIM,
        filmDetail: res.data.content,
      });
    } catch (e) {
      console.log("e: ", e);
    }
  };
};
