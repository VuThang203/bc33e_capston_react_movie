import { combineReducers } from "redux";
import { userReducer } from "./userReducer";
import { spinnerReducer } from "./spinnerReducer";
import { filmReducer } from "./filmReducer";

export const rootReducer = combineReducers({ userReducer, spinnerReducer, filmReducer });
