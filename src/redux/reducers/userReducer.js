import { SET_LOGIN } from "../constants/userConstants";
import { userInforLocal } from "../../service/local.service";

const initialState = {
  userInfor: userInforLocal.get(),
};

export let userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_LOGIN: {
      state.userInfor = payload;
      return { ...state };
    }
    default:
      return state;
  }
};
