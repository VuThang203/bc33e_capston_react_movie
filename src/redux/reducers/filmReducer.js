import { layThongTinChiTietPhim } from "redux/actions/filmAction"
import { XEM_CHI_TIET_PHIM } from "redux/constants/filmConstant"

const initialState = {
    filmDetail: {}
}

export let filmReducer = (state = initialState, action) => {
    switch (action.type) {

        case XEM_CHI_TIET_PHIM: {
            state.filmDetail = action.filmDetail
            return { ...state }
        }

        default:
            return state
    }
}
