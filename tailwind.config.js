/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      container: {
        padding: {
          DEFAULT: "1rem",
          sm: "2rem",
          lg: "4rem",
          xl: "10rem",
          "2xl": "6rem",
        },
      },
      height: {
        100: "50rem",
      },
      width: {
        100: "30rem",
      },
    },
  },
  plugins: [],
};
